# Fraud Detection V1 

This is a deployable code that queries the sql database everyday and runs a machine learning algorithim to detect fraudlant openings from the day before. It is an email service scripts that sends out a report of fraudelent cases once it scans the opens events on a daily basis.

`config.json` has the queries required for the code to run, in case the tables changes or the database has migrated please change the queries in this file to the equivilent ones. To change some settings in the code please change it in this config file

## How to install dependencies ?
just make sure the machine has python 2.7 or higher and run setup.py 

## How to run the code ? 
The following command will run a nohup process that will automatically keep running the process even when the cloud machine has exited. 
`sh START.sh` 

## How to view logs ?
`cd cloud_logs` 
`tail -f operation.log` 
or 
`nano operation.log`
