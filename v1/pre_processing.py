import pandas as pd
import numpy as np
import os
import pdb
import pickle
from query_data import query_db
import json
class pre_processor(object):
    '''
    This class is used to pre-process the data and extract the required features
    '''
    def __init__(self, retrain= False):
        self.config = self._read_config_file()
        if retrain:
            db = query_db()
            db.get_training_data()

    def _read_config_file(self):
        '''
        This methods raads a json configuration file to be used to setup the parametrs in the code.
        :return: json config format
        '''
        self.c_path = os.getcwd()
        if 'fraud_detection' in self.c_path :
            file_path = self.c_path+'/config.json'
        elif 'fraud_detection' not in self.c_path and 'wecheer' in self.c_path:
            self.c_path = self.c_path+'/fraud_detection'
            file_path = self.c_path+'/config.json'
        else:
            raise Exception('You are running the file not from a native directory')
        with open(file_path) as json_data:
            return json.load(json_data)

    def _get_logs(self):
        df = pd.read_csv(self.c_path+'/data/training_openers_activity_by_user.csv')
        return df

    def _pre_process(self,logs, train=False):
        '''
        Extact the required features
        :param df: Pandas dataframe that extracts
        :return:
        '''
        td_prev_opn_threshold = self.config['pre_process_metadata']['time_difference_max']
        df = logs.copy()
        df['ts'] = df['imageCaptureTimeStamp'].values
        df = df.sort_values(by=['usrId','ts'])
        df['year'] = pd.DatetimeIndex(df.date_time_image_capture).year
        df['month'] = pd.DatetimeIndex(df.date_time_image_capture).month
        df['day'] = pd.DatetimeIndex(df.date_time_image_capture).day
        df['hour'] =  pd.DatetimeIndex(df.date_time_image_capture).hour
        df['minute'] =  pd.DatetimeIndex(df.date_time_image_capture).minute
        df['weekYear'] = pd.DatetimeIndex(df.date_time_image_capture).weekofyear
        df['weekday'] = pd.DatetimeIndex(df.date_time_image_capture).weekday
        df['tm_diff_prv_opn'] = (df['ts'] - df.groupby('usrId')['ts'].shift(1)).fillna(td_prev_opn_threshold)
        df.tm_diff_prv_opn[df['tm_diff_prv_opn']  > td_prev_opn_threshold ]  = td_prev_opn_threshold
        aggregations = {
        'recordID':"count",
        'tm_diff_prv_opn':['mean','std']
        }
        feats = df.groupby([df.usrId,df.boxId,df.year,df.month,df.day,df.hour,df.minute,df.weekYear,df.weekday]).agg(aggregations).reset_index()
        feats.columns = ['usrId', 'boxId' , 'year', 'month', 'day', 'hour', 'minute', 'weekYear','weekday','open_counts', 'mean_td', 'std_td']
        feats = feats.fillna(0)
        if train :
            feats = feats[(feats.year == 2018) & (feats.month > 5)]
        feats = feats.sort_values(by=['usrId']).reset_index()
        return feats

    def _get_labels(self,feats):
        opn_threshold  = self.config['pre_process_metadata']['open_threshold']
        print 'threshold = ', opn_threshold
        blocked_users = pd.read_csv(self.c_path+'/data/confirmed_fraud_cases.csv').MAC_ID.values
        labels = []
        for index, row in feats.iterrows():
            if row.boxId in blocked_users and row.open_counts > opn_threshold:
                labels.append(1)
            else:
                labels.append(0)
        print 'Number of fraud events', sum(labels)
        print 'Total Number of events >', opn_threshold, 'bottles per minute', sum(feats.open_counts > opn_threshold)
        return np.array(labels)

    def pre_process_train(self, splitpercent = 0.8):
        dataset = self._pre_process(self._get_logs(), train = True)
        dataset['true_labels'] = self._get_labels(dataset)
        #dataset = dataset.drop(['boxId'], True)
        no_fraud_cases = sum(dataset['true_labels'].values)
        dataset_fraud = dataset[dataset.true_labels == 1]
        dataset_non_fraud = dataset[dataset.true_labels == 0].sample(int(no_fraud_cases*1.2))
        msk_fraud = np.random.rand(len(dataset_fraud)) < splitpercent
        msk_non_fraud = np.random.rand(len(dataset_non_fraud)) < splitpercent

        train = dataset_fraud[msk_fraud].append(dataset_non_fraud[msk_non_fraud])
        test_cases = dataset_fraud[~msk_fraud].append(dataset_non_fraud[~msk_non_fraud])

        train = train.drop(['index','usrId', 'boxId','year','month','weekYear'], True)
        test = test_cases.drop(['index','usrId', 'boxId','year','month','weekYear'], True)
        test_cases = test_cases.drop(['index'],True)

        y_train = train.true_labels.values
        x_train = train.drop(['true_labels'],True)
        feats_names = x_train.columns.values
        x_train = x_train.values
        y_test = test.true_labels.values
        x_test = test.drop(['true_labels'], True).values

        mu = np.mean(x_train)
        sd = np.std(x_train)

        with open(self.c_path+ '/mu_sd.pickle', 'wb') as f:
            pickle.dump([mu, sd], f)

        x_train = ( x_train -  mu)/sd
        x_test = (x_test - mu) / sd
        return x_train, y_train, x_test, y_test,feats_names, test_cases

    def pre_process_live(self, logs):
        df = self._pre_process(logs,train = False)
        try:
            cases = df.drop(['index'], True)
        except:
            cases = df
        prediction_cases = cases.drop(['usrId', 'boxId','year', 'month', 'weekYear'], True).values  # 'day','weekYear'
        with open(self.c_path+ '/mu_sd.pickle', 'rb') as f:
            mu, sd = pickle.load(f)
        prediction_cases = (prediction_cases - mu) / sd
        return prediction_cases, cases


if __name__ == "__main__":
    p = pre_processor(retrain = False)
    x_train, y_train, x_test, y_test,_,_ = p.pre_process_train()
    print x_train.shape, y_train.shape, x_test.shape, y_test.shape