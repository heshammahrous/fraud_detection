import mysql.connector
import pandas as pd
import json
import os
import time
import pdb
class query_db(object):
    def __init__(self):
        self.config = self._read_config_file()

    def _read_config_file(self):
        '''
        This methods raads a json configuration file to be used to setup the parametrs in the code.
        :return: json config format
        '''
        self.c_path = os.getcwd()
        if 'fraud_detection' in self.c_path :
            file_path = self.c_path+'/config.json'
        elif 'fraud_detection' not in self.c_path and 'wecheer' in self.c_path:
            self.c_path = self.c_path+'/fraud_detection'
            file_path = self.c_path+'/config.json'
        else:
            raise Exception('You are running the file not from a native directory')
        with open(file_path) as json_data:
            return json.load(json_data)

    def get_training_data(self, save=True):
        '''
        This method returns the get_opener_events_by_user query it reads a query from a config file
        :param: save: True/False default at True, if true it saves a csv in directory to avoid query time.
        :return: A pandas dataframe of the table queried
        '''
        db = mysql.connector.connect(
          host=self.config['database_connection']['host'],
          user=self.config['database_connection']['user'],
          passwd=self.config['database_connection']['pass'],
          port=self.config['database_connection']['port'] )
        cursor = db.cursor()
        print 'Quering New Table', self.config['queries']['get_training_data_opener_events_by_user']
        cursor.execute(self.config['queries']['get_training_data_opener_events_by_user'])
        df = pd.DataFrame(cursor.fetchall())
        desc = cursor.description
        cols = df.columns.values
        df.columns = [desc[i][0] for i in range(len(cols))]
        df['date_time_image_capture'] = pd.DatetimeIndex(pd.to_datetime(df.imageCaptureTimeStamp,unit='s'))
        if save:
            df.to_csv(self.c_path+'/data/training_openers_activity_by_user.csv', index= False)

    def get_daily_data(self):
        db = mysql.connector.connect(
            host=self.config['database_connection']['host'],
            user=self.config['database_connection']['user'],
            passwd=self.config['database_connection']['pass'],
            port=self.config['database_connection']['port'])
        cursor = db.cursor()
        days = 4
        query = self.config['queries']['daily_queries_for_prediction'] + ' and box.imageCaptureTimeStamp > ' + str(int(time.time()) - (days*24*60*60) )  + ' and box.imageCaptureTimeStamp < ' + str(int(time.time()) )
        print 'Quering New Data',query
        cursor.execute(query)
        df = pd.DataFrame(cursor.fetchall())
        desc = cursor.description
        cols = df.columns.values
        df.columns = [desc[i][0] for i in range(len(cols))]

        df['date_time_image_capture'] = pd.DatetimeIndex(pd.to_datetime(df.imageCaptureTimeStamp, unit='s'))
        df['year'] = pd.DatetimeIndex(df.date_time_image_capture).year
        df['month'] = pd.DatetimeIndex(df.date_time_image_capture).month
        df['day'] = pd.DatetimeIndex(df.date_time_image_capture).day
        df['hour'] = pd.DatetimeIndex(df.date_time_image_capture).hour
        df['minute'] = pd.DatetimeIndex(df.date_time_image_capture).minute
        return df

if __name__ == "__main__":
    q = query_db()
    print 'current directory', os.getcwd()
    if 'openers_activity_by_user.csv' not in os.listdir(os.getcwd()) :
        print 'querying new table'
        data = q.get_training_data()
    else:
        print 'table already saved reading csv'
        data = pd.read_csv('data/training_openers_activity_by_user.csv')
