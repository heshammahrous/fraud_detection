import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils import class_weight
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from pre_processing import pre_processor
import os
import pickle
import pandas as pd
import json
import pdb
class ML_model(object):
    def __init__(self,retrain=False):
        self.config = self._read_config_file()
        self.p = pre_processor(retrain)
        self.threshold =  self.config['machinelearning_metadata']['prediction_threshold']
        if 'production_model_V2.sav' in os.listdir(self.c_path) :
            # load the model from disk
            filename = self.c_path + '/production_model_V2.sav'
            self.model = pickle.load(open(filename, 'rb'))
        if retrain :
            self._train_new_production_model()
    def _read_config_file(self):
        '''
        This methods raads a json configuration file to be used to setup the parametrs in the code.
        :return: json config format
        '''
        self.c_path = os.getcwd()
        if 'fraud_detection' in self.c_path :
            file_path = self.c_path+'/config.json'
        elif 'fraud_detection' not in self.c_path and 'wecheer' in self.c_path:
            self.c_path = self.c_path+'/fraud_detection/v2'
            file_path = self.c_path+'/config.json'
        else:
            raise Exception('You are running the file not from a native directory')
        with open(file_path) as json_data:
            return json.load(json_data)

    def _train_new_production_model(self):
        x_train, y_train, x_test, y_test,feats_names,test_cases = self.p.pre_process_train()
        cw = class_weight.compute_class_weight('balanced', np.unique(y_train), y_train)
        cw = {0: cw[0], 1: cw[1]}
        RF = RandomForestClassifier(n_estimators = 500, max_depth= None, class_weight = cw, max_leaf_nodes= None, criterion = 'gini')
        RF.fit(x_train, y_train)
        y_predict = RF.predict_proba(x_test)
        pred = []
        for i in range(y_predict.shape[0]):
            if y_predict[i][1] > self.threshold:
                pred.append(1)
            else:
                pred.append(0)
        pred = np.array(pred)
        test_cases[pred==1].to_csv('suspected_cases_test.csv',index=False)
        C = confusion_matrix(y_test, pred)
        print 'Confusion Matrix'
        print C
        print 'Classiication Report'
        print classification_report(y_test, pred)
        print "Features sorted by their score:"
        print feats_names
        print RF.feature_importances_
        self.model = RandomForestClassifier(n_estimators=500, max_depth=None, class_weight=cw, max_leaf_nodes=None,
                                    criterion='gini')
        self.model.fit(np.concatenate((x_train,x_test),axis=0) , np.concatenate((y_train,y_test)))
        filename = self.c_path + '/production_model_V1.sav'
        pickle.dump(self.model, open(filename, 'wb'))

    def live_prediction(self, records):
        predicitons = self.model.predict_proba(records)
        pred = []
        for i in range(predicitons.shape[0]):
            if predicitons[i][1] > self.threshold:
                pred.append(1)
            else:
                pred.append(0)
        pred = np.array(pred)
        return pred

if __name__ == "__main__":
    ml = ML_model(True)
    import requests
    df = pd.read_csv('suspected_cases_test.csv')
    ids = df.usrId.values
    url = 'https://insights.wecheer.io/api/user_detail'
    payload = "{\n\t\"ids\" : [\n\t\t\t\""
    for i in range(len(ids)):
        if i != len(ids)-1 :
            payload = payload + ids[i] + "\",\n\t\t\t\""
        else:
            payload = payload + ids[i] + "\"\n\t\t],\n\t\"fields\" : [\n\t\t\t\"phone\",\n\t\t\t\"name\"\n\t\t]\n}"
    headers = {
        'Authorization': 'mSFbDdNXhZ3qdLQtALUxPTaWnAqiAdap',
        'Content-Type': 'application/json'
    }
    response = requests.request('POST', url, headers=headers, data=payload, allow_redirects=False)
    names_phone_numbers = pd.read_json(response.text, orient='index').reset_index(drop=True)
    df = df.merge(names_phone_numbers,left_on='usrId', right_on='_id',how='inner' )
    def is_not_ascii(string):
        return string is not None and any([ord(s) >= 128 for s in string])
    try:
        df = df[df['name'].apply(is_not_ascii)]
    except:
        pass
    df.to_csv('suspected_cases_test.csv', index=False, header = True, encoding= 'utf-8')
