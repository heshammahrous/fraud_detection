# Fraud Detection V2 

This is a deployable code that queries the sql database everyday and runs a machine learning algorithim to detect fraudlant openings from the day before. It is an email service scripts that sends out a report of fraudelent cases once it scans the opens events on a daily basis.

`config.json` has the queries required for the code to run, in case the tables changes or the database has migrated please change the queries in this file to the equivilent ones. To change some settings in the code please change it in this config file

# What is the new changes in V2 ? 

From now, please inspect the fraud emails and instead of sending them back with green and red labels as approve or reject any particular fraud event, please add them to this sheet, you have edit rights to it. Basically this sheet is the way you give feedback to the machine learning algorithm where it read the sheet and relearn the outcome of its previous prediction from your feedback. So far, there are 686 feedback cases we managed to get feedback from you and I already retrained the model with your feedback examples. The model was tested and the performance has dramatically improved scoring above 80%, which means less false positives and it should give you better results. As explained before https://docs.google.com/document/d/1YkRADXHhjcSExwN_y6A4TXhSws4IvqN1O18FthOFcdc/edit?usp=sharing the model looks into the timing evolution properties of the opens that includes number of opens and the time difference between each open and it compares it towards the population. However, it did not get us good results from the first run because we did not have any labels to define what is a fraud exactly but now we do.
Previously, you used to label the email reports in red and green and obviously this is hard to read in python so instead of labeling red and green I added a column for you called it “DECISION” this field should only have Y(confirmed fraud) or N(Not fraud). Going forward please inspect the email and populate more examples in the following google sheet so that the machine learning algo reads it everyday and learns the new labels, hence active learning is achieved and better performance while we go. And obviously the more examples or feedback you give the better results it should give you the next day or at least depending on the number of examples. So accordingly please do not delete anything from this sheet and you can only add more data. The google sheet ownership will be transferred to Mihai so anything related to core changes in authentication (for example transferring the sheet around or creating a new one) shall be handled by Mihai or whoever have access to the python script. I created a json configuration file where you can change some parameters like sheet ID and other nessary authentications in order for python script can access the google sheet.
https://docs.google.com/spreadsheets/d/1uqw1ZoxJaCu7vZ68uZEnfpqRlkQIKRlT9rGV7h4oPcc/edit?usp=sharing

## How to install dependencies ?
just make sure the machine has python 2.7 or higher and run `python2 setup.py` or `python setup.py`

## How to run the code ? 
The following command will run a nohup process that will automatically keep running the process even when the cloud machine has exited. 
`sh START.sh` 

## How to view logs ?
`cd cloud_logs` 
`tail -f operation.log` 
or 
`nano operation.log`

## How to change access and mainpulate access to the google sheet ? 
Currently the google sheet is being controlled from an API that I created on my own google account, you should create your own API by following the following link. 
https://gspread.readthedocs.io/en/latest/ and https://gspread.readthedocs.io/en/latest/oauth2.html 

Please follow these links to create your own API so that you have control over the sheet and access rights. 

**To configure the google sheet access in the code just go to the config.json file and do the nessary changes there.**

In the config file you will find: 

``
"google_sheet":
  {
    "sheetname": "1uqw1ZoxJaCu7vZ68uZEnfpqRlkQIKRlT9rGV7h4oPcc",
    "feeds": "https://spreadsheets.google.com/feeds",
    "drive": "https://www.googleapis.com/auth/drive",
    "credential_file": "wecheerFrauddetectionSheet-57178fd6db3c.json"
  }
``

* sheetname is found in the url link of the google sheet for exmample: https://docs.google.com/spreadsheets/d/1uqw1ZoxJaCu7vZ68uZEnfpqRlkQIKRlT9rGV7h4oPcc/edit#gid=0 
the sheetname should be: 1uqw1ZoxJaCu7vZ68uZEnfpqRlkQIKRlT9rGV7h4oPcc 

* feeds: Keep as is as instructed in the references mentioned above 
* drive: Keep as is as instructed in the references mentioned above 
* credential_file: This file is downloaded when going through the steps mentioned above it is needed so that the API can gain edit access to google sheet 


 
