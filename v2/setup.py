"""This module runs some setup required for using the package"""

from setuptools import setup, find_packages
import subprocess
import os


if __name__ == '__main__':

    ## Get the required package names
    with open("requirements.txt") as f:
        install_requires = f.read().splitlines()
    # RAW PACKAGE INSTALL 
    print "Installing python dependencies..."
    os.system('sudo pip2 install --upgrade pip')
    for req in install_requires:
        os.system('sudo pip2 install {} --user'.format(req))