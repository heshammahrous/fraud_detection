import time
from query_data import query_db
from pre_processing import pre_processor
from ML_models import ML_model
import requests
import pandas as pd
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders
import os, sys
import numpy as np
from re_train import retrain
import pdb

def send_email_attachement(attachment_path1, attachment_file_name1, attachment_path2, attachment_file_name2,  email_to):
    fromaddr = "frauddetectionwecheer@gmail.com"
    toaddr = email_to
    msg = MIMEMultipart()
    msg['From'] = 'FRAUD DETECTION ENGINE'
    msg['To'] = email_to
    msg['Subject'] = "Daily Fraud Report"

    body = "This is an automated email service, please find attached the last day of fraud report and the supporting logs that shows the opening events. For any questions please email Hesham at hesham@wecheer.io"

    msg.attach(MIMEText(body, 'plain'))

    filename1 = attachment_file_name1
    attachment1 = open(attachment_path1, "rb")

    filename2 = attachment_file_name2
    attachment2 = open(attachment_path2, "rb")

    part = MIMEBase('application', 'octet-stream')
    part.set_payload((attachment1).read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', "attachment; filename= %s" % filename1)
    msg.attach(part)

    part = MIMEBase('application', 'octet-stream')
    part.set_payload((attachment2).read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', "attachment; filename= %s" % filename2)
    msg.attach(part)

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(fromaddr, "Thisisme2")
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()

def send_email_msg(email_to):
    fromaddr = "frauddetectionwecheer@gmail.com"
    toaddr = email_to
    msg = MIMEMultipart()
    msg['From'] = 'FRAUD DETECTION ENGINE'
    msg['To'] = email_to
    msg['Subject'] = "Daily Fraud Report"

    body = "This is an automated email service, the fraud bot did not detect any fraudulent events of yesterday's activity. For any questions please email Hesham at hesham@wecheer.io"

    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(fromaddr, "Thisisme2")
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()


def get_name_phone_number(ids):
    url = 'https://insights.wecheer.io/api/user_detail'
    payload = "{\n\t\"ids\" : [\n\t\t\t\""
    for i in range(len(ids)):
        if i != len(ids)-1 :
            payload = payload + ids[i] + "\",\n\t\t\t\""
        else:
            payload = payload + ids[i] + "\"\n\t\t],\n\t\"fields\" : [\n\t\t\t\"phone\",\n\t\t\t\"name\"\n\t\t]\n}"
    headers = {
        'Authorization': 'mSFbDdNXhZ3qdLQtALUxPTaWnAqiAdap',
        'Content-Type': 'application/json'
    }
    response = requests.request('POST', url, headers=headers, data=payload, allow_redirects=False)
    return pd.read_json(response.text, orient='index').reset_index(drop=True)

def is_not_ascii(string):
    return string is not None and any([ord(s) >= 128 for s in string])
#flag = True
while True:
    #if flag:
    if time.gmtime().tm_hour == 00  and  time.gmtime().tm_min == 00 and time.gmtime().tm_sec >= 1 and time.gmtime().tm_sec <= 2:
        print time.asctime()  + ': starting a new process ...'
        sys.stdout.flush()
        q = query_db()
        sys.stdout.flush()
        logs  = q.get_daily_data()
        sys.stdout.flush()
        p = pre_processor()
        prediction_cases, cases = p.pre_process_live(logs)
        sys.stdout.flush()
        ML = ML_model()
        pred = ML.live_prediction(prediction_cases)
        sys.stdout.flush()
        df = cases[pred == 1]
        ids = df.usrId.values
        #emailsToSend = ['fraud@wecheer.io']
        emailsToSend = ['hesham.m.mahrous@gmail.com', 'aurele@wecheer.io', 'hesham@wecheer.io', 'giang.le@wecheer.io' , 'gert@wecheer.io']
        if len(df) > 0:
            names_phone_numbers = get_name_phone_number(ids)
            df = df.merge(names_phone_numbers, left_on='usrId', right_on='_id', how='inner')
            try:
                df = df.drop(['index','mean_td','std_td','_id'], True)
            except:
                df = df.drop(['mean_td', 'std_td', '_id'], True)
            sys.stdout.flush()
            ts = time.asctime()
            filename1 = 'report_' + ts + '.csv'
            filename2 = 'logs_' + ts + '.csv'
            fraud_boxID = np.unique(df.boxId.values).tolist()
            logs = logs[logs['boxId'].isin(fraud_boxID)]
            try :
                df.to_csv('daily_reports/' + filename1, index=False, header=True, encoding='utf-8')
                logs.to_csv('daily_reports/'+filename2,index=False, header= True, encoding= 'utf-8')
            except:
                df.to_csv('daily_reports/' + filename1, index=False, header=True)
                logs.to_csv('daily_reports/' + filename2, index=False, header=True)
            for e in emailsToSend :
                print 'sending report to',e
                send_email_attachement(os.getcwd()+'/daily_reports/'+filename1,filename1, os.getcwd()+'/daily_reports/'+filename2,filename2, e)
            time.sleep(5)
            sys.stdout.flush()

        else:
            print 'No fraud cases detected'
            for e in emailsToSend :
                send_email_msg(e)
        #flag = False
    elif time.gmtime().tm_hour == 06 and time.gmtime().tm_min == 00 and time.gmtime().tm_sec >= 1 and time.gmtime().tm_sec <= 2:
        try:
            Active_learn = retrain()
            Active_learn.retrain_model()
        except:
            print ('failed to train')
    else:
        time.sleep(1)