import pdb
import json
import os

import pandas as pd
import numpy as np
import pickle

import gspread
from oauth2client.service_account import ServiceAccountCredentials 

from query_data import query_db
from sklearn.utils import class_weight
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.ensemble import RandomForestClassifier


class retrain(object):
    
    def __init__(self, retrain= False):
        self.config = self._read_config_file()
        self.db = query_db()

    def _read_config_file(self):
        '''
        This methods raads a json configuration file to be used to setup the parametrs in the code.
        :return: json config format
        '''
        self.c_path = os.getcwd()
        if 'fraud_detection' in self.c_path :
            file_path = self.c_path+'/config.json'
        elif 'fraud_detection' not in self.c_path and 'wecheer' in self.c_path:
            self.c_path = self.c_path+'/fraud_detection'
            file_path = self.c_path+'/config.json'
        else:
            raise Exception('You are running the file not from a native directory')
        with open(file_path) as json_data:
            return json.load(json_data)

    def get_gsheet(self):
        sheet_name = self.config['google_sheet']['sheetname']
        credential_file=self.config['google_sheet']['credential_file']
        scope=[self.config['google_sheet']['feeds'], self.config['google_sheet']['drive']]
        credentials = ServiceAccountCredentials.from_json_keyfile_name(credential_file, scope)
        file = gspread.authorize(credentials)
        return file.open_by_key(sheet_name).sheet1


    def gsheet_to_dataframe(self, sheet):
        values = sheet.get_all_values()
        return pd.DataFrame(values[1:], columns = values[0])


    def pre_process(self, dataset, splitpercent = 0.9):
        dataset_fraud = dataset[dataset.DECISION == 1]
        dataset_non_fraud = dataset[dataset.DECISION == 0]
        msk_fraud = np.random.rand(len(dataset_fraud)) < splitpercent
        msk_non_fraud = np.random.rand(len(dataset_non_fraud)) < splitpercent

        train = dataset_fraud[msk_fraud].append(dataset_non_fraud[msk_non_fraud])
        test_cases = dataset_fraud[~msk_fraud].append(dataset_non_fraud[~msk_non_fraud])

        train = train.drop(['usrId', 'boxId','year','month','weekYear'], True).astype(int)
        test = test_cases.drop(['usrId', 'boxId','year','month','weekYear'], True).astype(int)

        x_train = train.drop(['DECISION'], True).values
        y_train = train.DECISION.values

        x_test = test.drop(['DECISION'], True).values
        y_test = test.DECISION.values

        mu = np.mean(x_train)
        sd = np.std(x_train)
        
        x_train = (x_train -  mu)/sd
        x_test = (x_test - mu) / sd
        with open(self.c_path+ '/mu_sd.pickle', 'wb') as f:
            pickle.dump([mu, sd], f)
        return x_train, y_train, x_test, y_test


    def get_user_logs(self, min_date,max_date,unique_users):
        mn_date = min_date - 3600
        mx_date = max_date + 3600

        df = self.db.get_user_logs(mn_date, mx_date)
        df = df[df['usrId'].isin(unique_users)].reset_index()

        td_prev_opn_threshold = self.config['pre_process_metadata']['time_difference_max']
        df['ts'] = df['imageCaptureTimeStamp'].values
        df = df.sort_values(by=['usrId', 'ts'])
        df['year'] = pd.DatetimeIndex(df.date_time_image_capture).year
        df['month'] = pd.DatetimeIndex(df.date_time_image_capture).month
        df['day'] = pd.DatetimeIndex(df.date_time_image_capture).day
        df['hour'] = pd.DatetimeIndex(df.date_time_image_capture).hour
        df['minute'] = pd.DatetimeIndex(df.date_time_image_capture).minute
        df['weekYear'] = pd.DatetimeIndex(df.date_time_image_capture).weekofyear
        df['weekday'] = pd.DatetimeIndex(df.date_time_image_capture).weekday

        df['tm_diff_prv_opn'] = (df['imageCaptureTimeStamp'] - df.groupby('usrId')['imageCaptureTimeStamp'].shift(1)).fillna(td_prev_opn_threshold)
        df.tm_diff_prv_opn[df['tm_diff_prv_opn']  > td_prev_opn_threshold ]  = td_prev_opn_threshold
        aggregations = {
            'recordID':"count",
            'tm_diff_prv_opn':['mean','std']
        }
        feats = df.groupby([df.usrId,df.boxId,df.year,df.month,df.day,df.hour,df.minute,df.weekYear,df.weekday]).agg(aggregations).reset_index()
        feats.columns = ['usrId', 'boxId' , 'year', 'month', 'day', 'hour', 'minute', 'weekYear','weekday','open_counts', 'mean_td', 'std_td']
        feats = feats.fillna(0)
        return feats

    def retrain_model(self):
        c_path = os.getcwd()
        model_file = c_path + "/production_model_V1.sav"
        # download data from google sheet
        sheet = self.get_gsheet()
        # Transform the google sheet into a pandas dataframe
        dataset = self.gsheet_to_dataframe(sheet)
        dataset["timeStamp"] = pd.to_datetime(dataset[["year", "month", "day", "hour", "minute"]]).apply(lambda d: int(d.timestamp()))  # pd.to_datetime(dataset[["year", "month", "day", "hour", "minute"]]).values.astype(np.int64) // 10 ** 9
        min_date = min(dataset["timeStamp"].values)
        max_date = max(dataset["timeStamp"].values)
        unique_users = np.unique(dataset["usrId"].values)
        feats = self.get_user_logs(min_date, max_date, unique_users)
        dataset['year'] = dataset['year'].astype(int)
        dataset['month'] = dataset['month'].astype(int)
        dataset['day'] = dataset['day'].astype(int)
        dataset['hour'] = dataset['hour'].astype(int)
        dataset['minute'] = dataset['minute'].astype(int)
        dataset = pd.merge(feats, dataset[['usrId', 'boxId', 'year', 'month', 'day', 'hour', 'minute', 'DECISION']],
                           how="left", on=['usrId', 'boxId', 'year', 'month', 'day', 'hour', 'minute'])
        dataset["DECISION"] = dataset.DECISION.fillna('N')
        dataset["DECISION"] = dataset["DECISION"].apply(lambda v: 1 if v == "Y" else 0)
        # pre-process data
        x_train, y_train, x_test, y_test = self.pre_process(dataset)
        # train model and rebuild
        model = pickle.load(open(model_file, 'rb'))
        model.fit(x_train, y_train)
        y_predict = model.predict_proba(x_test)
        pred = []
        for i in range(y_predict.shape[0]):
            if y_predict[i][1] > 0.5:
                pred.append(1)
            else:
                pred.append(0)
        pred = np.array(pred)
        C = confusion_matrix(y_test, pred)
        print 'Confusion Matrix'
        print C
        print 'Classiication Report'
        print classification_report(y_test, pred)
        print "Features sorted by their score:"
        print model.feature_importances_
        pickle.dump(model, open("production_model_V2.sav", 'wb'))


if __name__ == "__main__":
    re = retrain()
    re.retrain_model()